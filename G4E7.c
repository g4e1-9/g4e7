//GUIA MANEJO STRINGS EJERCICIO 7
//El usuario ingresará 5 nombres de personas y sus edades (número entero). 
//Luego de finalizar el ingreso, muestre en pantalla el nombre de la persona más joven.
//El ingreso se realiza de este modo: nombre y edad de la primera persona, luego nombre y edad de la segunda, etc...
//Nota: no hay que almacenar todos los nombres y todas las notas.

#include <stdio.h>
#include <string.h>


int main () {
	 
	char nombre[20]; //Declaro las variables.
	char joven[20];
	int i, comparador, edad;
	
	for(i=1;i<=5;i++)
	{
		printf("Ingrese el nombre N%d: ", i); //Solicito el ingreso de un nombre y una edad.
		scanf("%s", nombre);
		printf("Ingrese la edad N%d: ", i);
		scanf("%d", &edad);
		
        if ( i == 0)
        {
        comparador = edad; //Comparo las edades y detecto el string, y el nombre, del más joven.
        strcpy (joven, nombre);
        }
        if (edad < comparador)
        {
        strcpy(joven, nombre);
        comparador = edad;
        }
	}
	
	printf("La persona mas joven se llama %s.", joven); //Muestro en pantalla el nombre de la persona más joven.                         
    
	return 0;
}